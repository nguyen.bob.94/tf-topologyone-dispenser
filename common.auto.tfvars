##### Uncomment this variable to define the hostnames of VMs to deploy ######

#vm_names = ["VM01","VM02","VM03"]



##### Uncomment these variables to custom define the resource group name and region ######

#resource_group_name = "azure-resource-group-customname"
#resource_location = "australiaeast"



##### Uncomment these variables to define the network configuration resources required for the resources #####

#vnet_name = "azure-vnet01"
#vnet_address_space = ["10.0.0.0/16"]

#subnet_name = "subnetname"
#subnet_prefixes = ["10.0.0.0/24"]



##### Uncomment these variables to define the Azure Keyvault configurations #####

#key_vault_name = "contino-tftest-keyvault"
#key_vault_sku = "standard"
#key_permissions = ["get","list"]
#secret_permissions = ["get","list"]



##### Uncomment these variables to define the VM configurations #####

#vm_size = "Standard_D2_v4"
#os_disk_caching = "ReadWrite"
#os_disk_storage_account_type = "Standard_LRS"