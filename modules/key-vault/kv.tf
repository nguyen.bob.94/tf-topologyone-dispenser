resource "azurerm_key_vault" "kv" {
  location            = var.location
  name                = var.name
  resource_group_name = var.resource_group_name
  sku_name            = var.sku_name
  tenant_id           = var.tenant_id

  enabled_for_disk_encryption = true
}

# The Access policy configuration is not provided the modules. To acheive the configuration, this new resource "kvpolicy" is created
resource "azurerm_key_vault_access_policy" "kvpolicy" {
  key_vault_id = azurerm_key_vault.kv.id
  tenant_id = var.tenant_id
  object_id = var.object_id

  key_permissions = var.key_permissions
  secret_permissions = var.secret_permissions
}