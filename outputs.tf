output "vm_outputs" {
  value = [ for vms in module.vm[*] : {
    admin_username = local.vm_local_admin_username
    admin_password = local.vm_local_admin_password
    id = vms.vm-id 
    identity_id = vms.vm-name 
    }
  ]
}