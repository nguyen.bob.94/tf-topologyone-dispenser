# Required for provider
# Variables for VMs
variable "vm_names" {
  type = list
  default = ["VM01","VM02","VM03"]
}

# Variables for the Resource Group
variable "resource_group_name" {
  description = "The name for the Azure resource group to host all the Azure resources"
  type = string
  default = "topologyone-resourcegroup"
}

variable "resource_location" {
  description = "The region associated to all the Azure resources that will be deployed"
  type = string
  default = "australiaeast"
}

# Variables for the vnet
variable "vnet_name" {
  type = string
  default = "topologyone-vnet"
}

variable "vnet_address_space" {
  type    = list(any)
  default = ["10.0.0.0/16"]
}

# Variables for the subnet
variable "subnet_name" {
  type = string
  default = "topologyone-subnet"
}

variable "subnet_prefixes" {
  type    = list(any)
  default = ["10.0.0.0/24"]
}

# Variables for keyvault
variable "key_vault_name" {
  type = string
  default = "topologyone-keyvault"
}

variable "key_vault_sku" {
  type = string
  default = "standard"
}

variable "key_permissions" {
  type = list
  default = ["get","list"]
}

variable "secret_permissions" {
  type = list
  default = ["get","list"]
}

variable "vm_size" {
  type = string
  default = "Standard_D2_v4"
}

variable "vm_admin_username" {
  description = "Please enter the username for the local admin account for the VM(s). Default is `rootadmin`"
  type = string
}

variable "vm_admin_password" {
  description = "Please enter a password for the local admin account for the VM(s)"
  type = string
}

variable "os_disk_caching" {
  type = string
  default = "ReadWrite"
}

variable "os_disk_storage_account_type" {
  type = string
  default = "Standard_LRS"
}