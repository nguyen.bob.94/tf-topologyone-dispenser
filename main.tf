# Initiate Terraform with Azure
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.82.0"
    }
  }
}

# MANDATORY - This is a Microsoft Azure Provider configuration.
# Please use environment variables to define the Azure tenant and client details
# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/service_principal_client_secret#creating-a-service-principal-in-the-azure-portal
provider "azurerm" {
  features {}
}

# Azure config data used to obtain tenant ID information. This will be used for the properties of the "key-vault" module
data "azurerm_client_config" "current" {}

# Resource group for all the Azure resources to be deployed into.
resource "azurerm_resource_group" "resourcegroup" {
  name = var.resource_group_name
  location = var.resource_location
}

# Virtual Network for the subnet and VMs
resource "azurerm_virtual_network" "vnet" {
  name = var.vnet_name
  address_space = var.vnet_address_space
  resource_group_name = azurerm_resource_group.resourcegroup.name
  location = azurerm_resource_group.resourcegroup.location
}

# Subnet for the VMs
resource "azurerm_subnet" "subnet" {
  name = var.subnet_name
  address_prefixes = var.subnet_prefixes
  resource_group_name = azurerm_resource_group.resourcegroup.name
  virtual_network_name = azurerm_virtual_network.vnet.name
}

# Azure Key Vault - To be created via modules at ./modules/key-vault
module "key-vault" {
  source = "./modules/key-vault"
  name = var.key_vault_name
  resource_group_name = azurerm_resource_group.resourcegroup.name
  location = azurerm_resource_group.resourcegroup.location
  sku_name = var.key_vault_sku
  tenant_id = data.azurerm_client_config.current.tenant_id
  object_id = data.azurerm_client_config.current.object_id
  key_permissions = var.key_permissions
  secret_permissions = var.secret_permissions
}

# The local admin username and password the VMs are mandatory properties
# Using local variables will achieve applying the same credentials to all the vms to be deployed
locals {
  vm_local_admin_username = var.vm_admin_username
}

locals {
  vm_local_admin_password = var.vm_admin_password
}

# Module to create VMs
module "vm" {
  source = "./modules/vm"
  count = length(var.vm_names)
  location = azurerm_resource_group.resourcegroup.location
  name = var.vm_names[count.index]
  resource_group_name = azurerm_resource_group.resourcegroup.name
  size = var.vm_size
  caching = var.os_disk_caching
  storage_account_type = var.os_disk_storage_account_type
  subnet_id = azurerm_subnet.subnet.id

  admin_username = local.vm_local_admin_username
  admin_password = local.vm_local_admin_password
}