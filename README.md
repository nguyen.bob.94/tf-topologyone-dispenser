# Summary

The purpose of this Terraform configuration aims to automate the task of deploying 3 Azure VMs, an Azure Key Vault, as well as all the required resources that are dependant on the VMs. Using this Terraform configuration will deploy the following topology below.

<img src="https://labstorageaccount1.blob.core.windows.net/blob-content/topologyone.png" width="800" height="480">


#### Workflow

This configuration will call 2 modules. One will be used for creating 3 VM instances, while the other one will be used for creating the Azure Key Vault. In addition, the configuration will also utilse environment variables for defining the local admin credentials for each of the VMs created.

<img src="https://labstorageaccount1.blob.core.windows.net/blob-content/workflow.png" width="650" height="600">

# Prerequesites

To be able to utilise this Terraform configuration. You must have the following:

1. **Microsoft Azure account**: A cloud provider for Terraform to deploy its resources onto.
...https://azure.microsoft.com
2. **Terraform**: A CLI program for using Terraform on our computer.
...https://www.terraform.io/downloads.html (For latest version)
...https://community.chocolatey.org/packages/terraform (Via Chocolatey)
3. **Azure CLI**: Terraform CLI can only authenticate to Azure via Azure CLI.
...https://registry.terraform.io/providers/hashicorp/azuread/latest/docs/guides/azure_cli
...https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-windows?tabs=azure-cli (Windows)
...https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-macos (MacOS)
...https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-linux?pivots=apt (Linux)
4. **Git**: To obtain this repository to your local system.

# Setup Azure for Terraform

If this is the first time using Terraform with Azure, you must configure Azure to allow Terraform to authenicate to your tenant.

You will need to perform the following actions:
- Create a Service Principal for Terraform on Azure
- Generate a Secret for the Service Principal

This can be acheived by either using the [Azure AD portal](https://aad.portal.azure.com) under App Registrations, or using the Powershell CLI. For specific instructions on how to do this, please read https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/service_principal_client_secret#creating-a-service-principal-in-the-azure-portal

Obtain the values for the `ARM_CLIENT_ID`, `ARM_CLIENT_SECRET`, `ARM_SUBSCRIPTION_ID` and `ARM_TENANT_ID` from your Azure tenant and apply them to your environment variables on your computer. Instructions on how to do this is also mentioned from the URL above.

Below are examples of the environment variables and its value format to declare for the Azure tenancy details.

#### Powershell
```
$env:ARM_CLIENT_ID="00000000-0000-0000-0000-000000000000"
$env:ARM_CLIENT_SECRET="stringoftext"
$env:ARM_SUBSCRIPTION_ID="00000000-0000-0000-0000-000000000000"
$env:ARM_TENANT_ID="00000000-0000-0000-0000-000000000000"
```

#### Bash
```
export ARM_CLIENT_ID="00000000-0000-0000-0000-000000000000"
export ARM_CLIENT_SECRET="stringoftext"
export ARM_SUBSCRIPTION_ID="00000000-0000-0000-0000-000000000000"
export ARM_TENANT_ID="00000000-0000-0000-0000-000000000000"
```

#### Using Terraform Cloud

Alternatively, if you wish to declare these environment variables using Terraform Cloud workspaces instead, you may do so. However, keep in mind that you will need to clone this repo and upload it to your own Gitlab account, then set up a VCS connection between Gitlab and Terraform Cloud. Refer to the 2 URLs below for instructions on how to set up a Terraform Cloud workspace.
- https://www.terraform.io/docs/cloud/vcs/gitlab-com.html
- https://www.terraform.io/docs/cloud/workspaces/variables.html

# Input Variables

The list are the available input variables for this configuration, as well as its default values. 

Variables with no default variables will be required to be defined as environment variables or manual input.

|Variable|Type|Default Value|
|--------|----|-------------|
| vm_names | list | ["VM01","VM02","VM03"]
| resource_group_name | string | "topologyone-resourcegroup" |
| resource_location | string | australiaeast |
| vnet_name | string | "topologyone-vnet" |
| vnet_address_space | list | ["10.0.0.0/16"] |
| subnet_name | string | "topologyone-subnet" |
| subnet_prefixes | list | ["10.0.0.0/24"] |
| key_vault_name | string | "topologyone-keyvault" |
| key_vault_sku | string | "standard" |
| key_permissions | list | ["get","list"] |
| secret_permissions | list | ["get","list"] |
| vm_size | string | "Standard_D2_v4" |
| os_disk_caching | string | "ReadWrite" |
| os_disk_storage_account_type | string | "Standard_LRS" |
| vm_admin_username | string | N/A |
| vm_admin_password | string | N/A |

# Overriding the default Variables

Overriding the default variables can be acheived through several ways. One method is to be parse environment variables into the Terraform configuration. Another option is to define the variables with your desired values in a `common.auto.tfvars` file.

For best practice, declare any sensitive variables using environment variables so it does not check in when adding into VCS. For non sensitive variables, use tfvars.

#### Declaring an enviroment variable

Lets consider an example of defining a environment variable for the local admin credentials for the VMs. In Powershell, you would enter the following.

```
# Powershell
$env:TF_VAR_vm_admin_username = "rootadmin"
$env:TF_VAR_vm_admin_password = "Password123!"
```
For bash on the otherhand, you would enter the following.


```
# Bash
export TF_VAR_vm_admin_username="rootadmin"
export TF_VAR_vm_admin_password="Password123!"
```

#### Defining Terraform variable using tfvars

Within the Terraform configuration directory, open up a text editor and edit the `common.auto.tfvars`

Define the variables needed. Be sure to take into consideration of type of variables used,

In this example, we want to override the default variables for ["VM01","VM02","VM03"] so that Terraform creates the VMs with the hostnames ["WINSERVERAPP01","WINSERVERAPP02","WINSERVERCA01"] instead.
```
vm_names = ["WINSERVERAPP01","WINSERVERAPP02","WINSERVERCA01"]
```

# Output Variables

The vm module already contains an `output.tf` file that extracts the `id`, `name` and `identity_id` information of a VM created from the module.

Another `output.tf` file will also be present within the root directory of the configuration. This output file uses a for loop to nest the output variables from the module, in order to provide an output of the information of each VM that is deployed, including details such as local admin credentials, VM id and name per instance. 

These details can then be provided to the end user(s) who will be utilising the VMs.

**Note**: Outputs will only become available after applying the Terraform plan.

# Quick Start
1. Clone this repository.
`git clone https://gitlab.com/nguyen.bob.94/tf-topologyone-dispenser.git`

2. Using your desired shell, navigate into the repo directory.
`cd ./TF-Topologyone-Dispenser`

3. Initiate the directory with Terraform
`terraform init`

4. Declare your environment variables for the local admin credentials. (Use the example from earlier)

5. Plan the configuration
`terraform plan`

6. Apply the configuration. Enter (yes) when prompted.
`terraform apply`

7. When you're done with the deployment, feel free to play around with it. Alternatively, destroy it.
`terraform destroy`

